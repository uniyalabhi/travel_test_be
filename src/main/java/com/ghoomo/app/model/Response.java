package com.ghoomo.app.model;

import java.util.Date;

public class Response {

	private String message;
	private Object data;
	private Date date;
	private Boolean status;

	public Response(String message, Object data, Date date, Boolean status) {
		super();
		this.message = message;
		this.data = data;
		this.date = date;
		this.status = status;
	}

	public Response(String message, Object data, Date date) {
		super();
		this.message = message;
		this.data = data;
		this.date = date;
	}
	
	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Response [message=" + message + ", data=" + data + ", date=" + date + ", status=" + status + "]";
	}

}
