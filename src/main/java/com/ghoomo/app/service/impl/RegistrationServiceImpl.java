package com.ghoomo.app.service.impl;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.cert.PKIXRevocationChecker.Option;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ghoomo.app.entity.UserDetail;
import com.ghoomo.app.model.UserDetailDTO;
import com.ghoomo.app.repository.RegistrationRepository;
import com.ghoomo.app.service.RegistrationService;
import com.google.common.io.Files;

@Service
public class RegistrationServiceImpl implements RegistrationService {

	@Autowired
	private RegistrationRepository registrationRepository;

	@Override
	public UserDetailDTO getRegisterDetails(String userId) {

		UserDetailDTO userDetailDTO = new UserDetailDTO();
		Optional<UserDetail> userDetail = registrationRepository.findByUserName(userId);
		if(userDetail.isPresent()) {
			BeanUtils.copyProperties(userDetail.get(), userDetailDTO);
			return userDetailDTO;
		}
		return null;
	}

	@Override
	public List<UserDetail> getAllUserDetails() {
		return registrationRepository.findAll();
	}

	@Override
	public UserDetailDTO save(UserDetailDTO userDetailDTO) {
		UserDetail userDetail = new UserDetail();

		BeanUtils.copyProperties(userDetailDTO, userDetail);
		userDetail = registrationRepository.save(userDetail);
		BeanUtils.copyProperties(userDetail, userDetailDTO);
		return userDetailDTO;
	}

	@Override
	public UserDetailDTO update(UserDetailDTO userDetailDTO) {

		Optional<UserDetail> userDetailOp = registrationRepository.findByUserName(userDetailDTO.getUserName());

		if (userDetailOp.isPresent()) {
			UserDetail userDetail = userDetailOp.get();
			userDetail.setFirstName(userDetailDTO.getFirstName());
			userDetail.setLastName(userDetailDTO.getLastName());
			userDetail.setMobileNo(userDetailDTO.getMobileNo());
			userDetail.setCity(userDetailDTO.getCity());
			userDetail.setCountry(userDetailDTO.getCountry());
			userDetail = registrationRepository.save(userDetail);
			BeanUtils.copyProperties(userDetail, userDetailDTO);
			return userDetailDTO;
		}

		return null;

	}

}
