package com.ghoomo.app.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ghoomo.app.service.FileService;

@Service
@PropertySource("classpath:message.properties")
public class FileServiceImpl implements FileService {

	@Value("${profile.image.location}")
	private String imageLocation;

	@Override
	public String uploadImage(MultipartFile file) {
		Path rootLocation = Paths.get(System.getProperty("user.dir").concat(imageLocation));

		System.out.println("name:" + file.getOriginalFilename());
		System.out.println(imageLocation);
		System.out.println("rootlocation:" + rootLocation.toUri());
		try {
			if (! Files.exists(rootLocation))
				Files.createDirectories(rootLocation);

			Files.copy(file.getInputStream(), rootLocation.resolve(file.getOriginalFilename()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "Uploaded";
	}

}
