package com.ghoomo.app.service;

import java.util.List;

import javax.validation.Valid;

import com.ghoomo.app.entity.UserDetail;
import com.ghoomo.app.model.UserDetailDTO;

public interface RegistrationService {

	public UserDetailDTO getRegisterDetails(String userName);

	public List<UserDetail> getAllUserDetails();

	public UserDetailDTO save(@Valid UserDetailDTO userDetailDTO);

	public UserDetailDTO update(UserDetailDTO userDetailDTO);

}
