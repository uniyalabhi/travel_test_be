package com.ghoomo.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.multipart.MultipartFile;

import com.ghoomo.app.entity.UserDetail;
import com.ghoomo.app.model.Response;
import com.ghoomo.app.model.UserDetailDTO;
import com.ghoomo.app.service.FileService;
import com.ghoomo.app.service.RegistrationService;
import com.ghoomo.app.utils.ApplicationConstants;

@RestController
@RequestMapping("/register")
@CrossOrigin(origins = "http://localhost:3000")
public class RegistrationController {

	@Autowired
	private RegistrationService registrationService;
	
	@Autowired
	private FileService fileService; 

	@GetMapping("/get/{userId}")
	public Response getRegisterDetails(@PathVariable String userId) {
		UserDetailDTO userDetailDto =  registrationService.getRegisterDetails(userId);
		String message = (userDetailDto != null)? ApplicationConstants.DATA_FOUND: ApplicationConstants.NO_DATA_FOUND;
		return new Response(message, userDetailDto, null);
	}

	@GetMapping("/get")
	public Response getAllUserDetails() {
		List<UserDetail> userDetailDto =  registrationService.getAllUserDetails();
		String message = (!userDetailDto.isEmpty())? ApplicationConstants.DATA_FOUND: ApplicationConstants.NO_DATA_FOUND;
		
		return new Response(message, userDetailDto, null);
	}
	
	@PostMapping("/save")
	public Response save(@RequestBody UserDetailDTO userDetailDTO) {
		UserDetailDTO userDetailDto =  registrationService.save(userDetailDTO);
		String message = (userDetailDto != null)? ApplicationConstants.DATA_SAVED : ApplicationConstants.NO_DATA_FOUND;
		return new Response(message, userDetailDTO, null);
	}
	
	@PutMapping("/update")
	public Response update(@RequestBody UserDetailDTO userDetailDTO) {
		UserDetailDTO userDetailDto = registrationService.update(userDetailDTO);
		String message = (userDetailDto != null)? ApplicationConstants.DATA_UPDATED: ApplicationConstants.NO_DATA_FOUND;
		return new Response(message, userDetailDto, null);
	}
	
	@PostMapping("/upload")
	public Response uploadImage(@RequestParam("file") MultipartFile file) {
		String userDetailDto = fileService.uploadImage(file);
		String message = (userDetailDto != null)? ApplicationConstants.DATA_UPDATED: ApplicationConstants.NO_DATA_FOUND;
		return new Response(message, userDetailDto, null);
	}
	
}
