package com.ghoomo.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ghoomo.app.entity.UserDetail;

@Repository
public interface RegistrationRepository extends JpaRepository<UserDetail, Long>{

	Optional<UserDetail> findByUserName(String userName);
	
}
