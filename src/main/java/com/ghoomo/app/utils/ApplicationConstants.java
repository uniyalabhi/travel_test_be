package com.ghoomo.app.utils;

public class ApplicationConstants {

	public static String DATA_FOUND = "Data Found Successfully";
	public static String NO_DATA_FOUND = "No Data Found";
	public static String DATA_SAVED = "Data Saved Successfully";
	public static String DATA_UPDATED = "Data Updated Successfully";

}
